const express = require('express');
const app = express();

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.use(express.static('public'));

app.listen(process.env.PORT, function () {
    console.log(`App listening on port : `, process.env.PORT);
});