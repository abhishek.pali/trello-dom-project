var apiKey = '042ad0a4e07ee114cc5df4534c75a0bc';
var apiToken = '012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f';

fetch(`https://api.trello.com/1/boards/ROV2ntBr/lists?key=${apiKey}&token=${apiToken}`, {
    method: 'GET'
  })
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(function(listsJSON) {

    var listId = listsJSON[1].id;
    
    fetch(`https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${apiToken}`, {
        method: 'GET'
      })
      .then(response => {
        console.log(
          `Response: ${response.status} ${response.statusText}`
        );
        return response.json();
      })
      .then(function(cardsJSON) {
        
        let cardIdArr = [];
        let paragraphIdArr = [];
        for (let index = 0; index < cardsJSON.length; index++)
          {
            var list = document.getElementById("list");
            var tagId = "card" + index + "-aTag";
            var anchorTag = document.createElement("a");
            anchorTag.setAttribute("id", tagId);
            cardIdArr.push(tagId);

            var cardDiv = document.createElement("div");
            cardId = "card-" + index;
            cardDiv.setAttribute("id", cardId);
            cardDiv.setAttribute("class", "max-w-xs");
            cardDiv.classList.add("rounded");
            cardDiv.classList.add("overflow-hidden");
            cardDiv.classList.add("shadow-lg");
            cardDiv.classList.add("bg-white");
            cardDiv.classList.add("m-2");

            var paraForCard = document.createElement("p");
            var paraId = cardsJSON[index].id;
            paraForCard.setAttribute("id", paraId);
            paragraphIdArr.push(paraId);
            paraForCard.setAttribute("class", "text-gray-700");
            paraForCard.classList.add("text-base");
            paraForCard.classList.add("p-2");

            paraForCard.innerHTML = cardsJSON[index].name;
            cardDiv.appendChild(paraForCard);
            anchorTag.appendChild(cardDiv);
            list.appendChild(anchorTag);
          }

          var list = document.getElementById("list");
          var addCardAnchorTag = document.createElement("a");
          addCardAnchorTag.setAttribute("id", "add-card-aTag");

          var addCardDiv = document.createElement("div");
          addCardDiv.setAttribute("id", "add-card")
          addCardDiv.setAttribute("class", "w-4/5");
          addCardDiv.classList.add("rounded");
          addCardDiv.classList.add("overflow-hidden");
          addCardDiv.classList.add("shadow-lg");
          addCardDiv.classList.add("bg-white");
          addCardDiv.classList.add("hover:bg-gray-400");
          addCardDiv.classList.add("m-2");

          var paraForAddCard = document.createElement("p")
          paraForAddCard.setAttribute("id", listId);
          paraForAddCard.setAttribute("class", "text-gray-700");
          paraForAddCard.classList.add("text-base");
          paraForAddCard.classList.add("p-1");

          paraForAddCard.innerHTML = "+ Add another Card";
          addCardDiv.appendChild(paraForAddCard);
          addCardAnchorTag.appendChild(addCardDiv);
          list.appendChild(addCardAnchorTag);

          return [cardsJSON, cardIdArr, paragraphIdArr, listId];
      })
      .then(function(arr) {
          
          var body = document.getElementsByTagName("body");

          var modalContainerDiv = document.createElement("div");
          modalContainerDiv.setAttribute("id", "modal-container");
          modalContainerDiv.setAttribute("class", "w-screen");
          modalContainerDiv.classList.add("flex");
          modalContainerDiv.classList.add("items-center");
          modalContainerDiv.classList.add("justify-center");
          modalContainerDiv.classList.add("invisible");
  
          var modalDiv = document.createElement("div");
          modalDiv.setAttribute("id", "modal");
          modalDiv.setAttribute("class", "w-1/2");
          modalDiv.classList.add("rounded-lg");
          modalDiv.classList.add("border-4");
          modalDiv.classList.add("border-white");
          modalDiv.classList.add("border-solid");
          modalDiv.classList.add("bg-red-500");
          modalDiv.classList.add("z-10");
  
          var crossContainerDiv = document.createElement("div");
          crossContainerDiv.setAttribute("id", "cross-container");
          crossContainerDiv.setAttribute("class", "border-b");
          crossContainerDiv.classList.add("border-white");
          crossContainerDiv.classList.add("text-white");
  
          var crossDiv = document.createElement("div");
          crossDiv.setAttribute("id", "cross-sign");
          crossDiv.setAttribute("class", "p-4");
          crossDiv.innerHTML = "X";

          var editNameTag = document.createElement("h1");
          editNameTag.setAttribute("id", "edit-name-tag");
          editNameTag.innerHTML = "Edit the name of the card in the input box below : ";
          editNameTag.setAttribute("class", "p-2");
          editNameTag.classList.add("text-black");

          var inputDiv = document.createElement("div");
          inputDiv.setAttribute("class", "p-2");
  
          var input = document.createElement("input");
          input.setAttribute("id", "input-for-editing-card");
          input.setAttribute("class", "bg-white");
          input.classList.add("p-2");
          input.classList.add("pl-4");
          input.classList.add("w-full");
          input.classList.add("text-black");
          input.classList.add("text-lg");
  
          var submitButton = document.createElement("button");
          submitButton.setAttribute("class", "mt-4");
          submitButton.classList.add("mb-4");
          submitButton.classList.add("ml-2");
          submitButton.classList.add("p-2")
          submitButton.classList.add("bg-white");
          submitButton.classList.add("rounded");
          submitButton.classList.add("submit-button");
          submitButton.innerHTML ="Submit";

          var deleteButton = document.createElement("button");
          deleteButton.setAttribute("class", "mt-4");
          deleteButton.classList.add("mb-4");
          deleteButton.classList.add("ml-10");
          deleteButton.classList.add("p-2")
          deleteButton.classList.add("bg-white");
          deleteButton.classList.add("rounded");
          deleteButton.classList.add("delete-button");
          deleteButton.innerHTML ="Delete Card";
  
          crossContainerDiv.appendChild(crossDiv);
          modalDiv.appendChild(crossContainerDiv);
          modalDiv.appendChild(editNameTag);
          inputDiv.appendChild(input);
          modalDiv.appendChild(inputDiv);
          modalDiv.appendChild(submitButton);
          modalDiv.appendChild(deleteButton);
  
          modalContainerDiv.appendChild(modalDiv);
          body[0].appendChild(modalContainerDiv);

          var addCardPara, addCardParaId;

          var paraForAddCard = document.getElementById(arr[3]);

          paraForAddCard.addEventListener('click', function(e) {

            addCardPara = e.target;
            addCardParaId = e.target.getAttribute("id");
            modalContainerDiv.setAttribute("class", "visible");
            modalContainerDiv.setAttribute("id", addCardParaId + "modal")
            var input = document.getElementById("input-for-editing-card");
            input.value = "";
            submitButton.setAttribute("id", addCardParaId);
            deleteButton.classList.add("invisible");
          });
     
          for (let index = 0; index < arr[1].length; index++)
            {
              var anchorTag = document.getElementById(arr[1][index]);
              anchorTag.addEventListener("click", eventFunction);
            }

            var targetId, targetElement;

            function eventFunction(e) {

              targetElement = e.target;
              targetId = e.target.getAttribute("id");
              modalContainerDiv.setAttribute("class", "visible");
              modalContainerDiv.setAttribute("id", targetId + "modal");
              var paraForCard = document.getElementById(targetId);
              var input = document.getElementById("input-for-editing-card");
              input.value = paraForCard.innerHTML;
              
            }
            
              submitButton.addEventListener("click", function(e) {
                
                if (addCardPara !== undefined)
                  {
                    var input = document.getElementById("input-for-editing-card");
                    var inputValue = input.value;

                    fetch(`https://api.trello.com/1/cards?idList=${e.target.id}&name=${inputValue}&key=${apiKey}&token=${apiToken}`, {
                          method: 'POST'
                        })
                          .then(response => {
                            console.log(
                              `Response: ${response.status} ${response.statusText}`
                            );
                            return response.text();
                          })
                          .then(function(text) {
                            
                            addCardPara.innerHTML = inputValue;
                            addCardPara.parentElement.classList.remove("w-4/5");
                            addCardPara.parentElement.classList.add("max-w-xs");
                          
                            var modalContainerDiv = document.getElementById(e.target.parentElement.parentElement.getAttribute("id"));
                            modalContainerDiv.setAttribute("class", "invisible");

                            console.log(`Added new card : \n ${text}`);
                            location.reload();
                          })
                          .catch(function(err) {
                            console.error(err);
                          });    
                  }
                else
                  {
                    var input = document.getElementById("input-for-editing-card");
                    var inputValue = input.value;
    
                    fetch(`https://api.trello.com/1/cards/${targetId}?name=${inputValue}&key=${apiKey}&token=${apiToken}`, {
                          method: 'PUT',
                          headers: {
                            'Accept': 'application/json'
                          }
                        })
                          .then(function(response) {                       
                              console.log(
                              `Response: ${response.status} ${response.statusText}`
                            );
                            return response.text();
                          })
                          .then(function(text) {
    
                            var paraForCard = document.getElementById(targetId);
                            paraForCard.innerHTML = inputValue;
                          
                            var modalContainerDiv = document.getElementById(e.target.parentElement.parentElement.getAttribute("id"));
                            modalContainerDiv.setAttribute("class", "invisible");
    
                            console.log(`Updated Card : \n ${text}`);
                          })
                          .catch(function(err) {
                            console.error(err);
                          });       
                  }
                });

                deleteButton.addEventListener("click", function(e) {

                    var list = document.getElementById("list");
                    
                    fetch(`https://api.trello.com/1/cards/${targetId}?key=${apiKey}&token=${apiToken}`, {
                          method: 'DELETE'
                          })
                          .then(response => {
                            console.log(
                              `Response: ${response.status} ${response.statusText}`
                            );
                            return response.text();
                          })
                          .then(function(text) {
                            
                            list.removeChild(targetElement.parentElement.parentElement);

                            var modalContainerDiv = document.getElementById(e.target.parentElement.parentElement.getAttribute("id"));
                            modalContainerDiv.setAttribute("class", "invisible");
                            
                            console.log(`Removed a card : \n ${text}`);
                          })
                          .catch(function(err) {
                            console.error(err);
                          });    
                });

                crossDiv.addEventListener("click", function() {
                  var modalContainerDiv = document.getElementById(targetId + "modal") || document.getElementById(addCardParaId + "modal");
                  modalContainerDiv.setAttribute("class", "invisible");
                });    
      })
      .catch(function(err) {
        console.error(err);
      });
  })
  .catch(function(err) {
    console.error(err);
  });


